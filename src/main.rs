// 1e 01 1e 01 separator per line of text
// 7f 31 00 at the end of each sentence
// 00 at the end of the entire file
use once_cell::sync::Lazy;
use regex::Regex;
use std::str::FromStr;
use strum::{EnumString, EnumVariantNames, VariantNames};

static AVATAR: Lazy<Regex> = Lazy::new(|| {
    let expr = Avatar::VARIANTS.join("|");
    Regex::new(&expr).unwrap()
});

#[derive(EnumString, EnumVariantNames, Debug)]
enum Avatar {
    Carbuncle,
    Garuda,
    Leviathan,
    Ramuh,
    Shiva,
    Ifrit,
    Titan,
    Fenrir,
    Diabolos,
}

#[derive(EnumString, EnumVariantNames, Debug)]
enum AvatarWard {}

#[derive(EnumString, EnumVariantNames, Debug)]
enum AvatarRage {
    JudgmentBolt,
    ChaoticStrike,
}

fn main() {
    println!("Hello, world!");
    let test1 = "Ramuh readies Judgment Bolt.";
    if let Some(captures) = AVATAR.captures(test1) {
        let variant_name = &captures[0];
        let variant = Avatar::from_str(variant_name).unwrap();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn blah() {
        let test1 = "Ramuh readies Judgment Bolt.";
        let test2 = "Ramuh readies Chaotic Strike.";
        assert_eq!(test1, test2);
    }
}
